<?php
class Dashboards extends CI_Controller{
    function __construct() {
        parent::__construct();
        if ($this->session->userdata('user_id') == '' && $this->session->userdata('user_name') == '') {
            redirect('Logins');
        }
        $this->load->model(array('Dashboard'), '', TRUE);
       
    }
    public function index()
    {
        $data['title']="Dashboards";
        $this->load->view('Dashboards/index');
    }

    public function access_denied(){
        $data['title']="Access Denied";
        $this->load->view("access_denied",$data);
    }
}